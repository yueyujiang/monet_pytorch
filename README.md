# MONet-pytorch
This is a pytorch implementation of [MONet: Unsupervised Scene Decomposition and Representation](https://arxiv.org/abs/1901.11390)  
Followings are five random examples from testing set.

## Masks for each slots 
Different lines for different samples and different columns for different slots

![mask image](./demo_img/mask.png) 

## Masked reconstruction mixtures

![mask image](./demo_img/masked_img.png) 

## reconstruction mixtures
The first line is the ground truth mixtures and the second line is the reconstruction mixtures

![mask image](./demo_img/recon_img.png) 